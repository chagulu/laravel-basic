<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FormSubmit;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/load-form',[FormSubmit::class,'loadForm']);
Route::post('/get-formdata',[FormSubmit::class,'getFormData'])->name('getFormData');